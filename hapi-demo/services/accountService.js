const assert = require('assert');
const i18n = require('../helpers/i18nHelper');
const cryptoHelper = require('../helpers/cryptoHelper');
const logger = require('../helpers/logHelper');

/**
 * Delete User
 */
 exports.getUser = (userId, acl) => new Promise((resolve, reject) => {
   assert(userId, i18n('services.accountService.missingUserId'));
   acl.getUserById(userId, function(error, user) {
     delete user.pwd;
     delete user.pwdSalt;
     resolve(user);
   });
});

/**
 * Create a user
 * @param userPayload {email, password, role}
 */

exports.createUser = (userPayload, acl) => new Promise((resolve, reject) => {
  assert(userPayload, i18n('services.accountService.missingUserPayload'));
  const userData = Object.assign({}, userPayload); // clone data so that we can alter it as needed
  delete userData.password;

  acl.getUserByUsername(userPayload.username, function(error, foundUser) {
      if (error) {
        reject(i18n('db.error'));
      }
      if (foundUser) {
        reject(i18n('services.accountService.userNameExists'));
        return;
      }
      cryptoHelper
        .hashString(userPayload.password)
        .then(({ hash, salt }) => {
          userData.pwd = hash;
          userData.pwdSalt = salt;
          acl.addUser(userData, function(error, user) {
            delete user.pwd;
            delete user.pwdSalt;
            acl.addUserRoles(user.id, 'user');
            resolve(user);
          });
        })
        .catch((dbErr) => {
          logger.error(dbErr);
          reject(i18n('db.error'));
        });

  });
});

/**
 * Update User
 */
 exports.updateUser = (userPayload, acl) => new Promise((resolve, reject) => {
   assert(userPayload, i18n('services.accountService.missingUserPayload'));
   const userData = Object.assign({}, userPayload); // clone data so that we can alter it as needed
   if (userPayload.password) {
     delete userData.password;
     delete userData.id;
     cryptoHelper
       .hashString(userPayload.password)
       .then(({ hash, salt }) => {
         userData.pwd = hash;
         userData.pwdSalt = salt;
         acl.updateUser(userPayload.id, userData, function(error, user) {
           resolve(user);
         });
       })
       .catch((dbErr) => {
         logger.error(dbErr);
         reject(i18n('db.error'));
       });
   }
   else {
     acl.updateUser(userPayload.id, userData, function(error, user) {
       resolve(user);
     });
   }
 });


 /**
  * Delete User
  */
  exports.deleteUser = (userId, acl) => new Promise((resolve, reject) => {
    assert(userId, i18n('services.accountService.missingUserId'));
    acl.deleteUser(userId, function(error, user) {
      resolve(user);
    });
 });
