const assert = require('assert');
const i18n = require('../helpers/i18nHelper');

const cryptoHelper = require('../helpers/cryptoHelper');
const logger = require('../helpers/logHelper');

exports.findSessionUser = (sessionData, acl) =>
  new Promise((resolve, reject) => {
      acl.getUserById(sessionData.id, function ( error, user ) {
         if (error) {
           reject(error);
         }
         else {
           resolve(user);
         }
      });
  });


/**
 * Authenticate a user by its username and plain password
 * @param username
 * @param password
 */
exports.authenticate = (username, password, acl) => new Promise((resolve, reject) => {
  assert(username, i18n('services.sessionService.missingUsername'));
  assert(password, i18n('services.sessionService.missingPassword'));

  const invalidMsg = () => {
    reject(i18n('services.sessionService.wrongUsernamePassword'));
  };
  acl.getUserByUsername(username, function ( error, userDetails ) {
     if (error) {
       invalidMsg();
       return;
     }
     else {
       if (!userDetails) {
         invalidMsg();
         return;
       }
      const user = userDetails;
      cryptoHelper
        .hashStringWithSalt(password, user.pwdSalt)
        .then((hashData) => {
          // Password invalid
          if (user.pwd !== hashData.hash) {
            invalidMsg();
            return;
          }

          // Sanitize user and return
          delete user.pwd;
          delete user.pwdSalt;
          delete user.resetPwdToken;
          delete user.resetPwdTokenExpires;

          resolve(user);
        })
        .catch((ex) => {
          logger.error(ex);
          invalidMsg();
        });
      }
    });
});
