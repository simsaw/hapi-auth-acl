'use strict';

const Hapi = require('hapi');
const Good = require('good');
const AclAuth = require('hapi-auth-acl');

const boom = require('boom');
const jwtHelper = require('./helpers/jwtHelper');
const sessionService = require('./services/sessionService');
const accountService = require('./services/accountService');

// bring your own validation function
var validate = function (decoded, request, callback) {
    const acl = request.server.plugins['hapi-auth-acl'].acl;
    if (decoded && decoded.id) {
      acl.getUserById(decoded.id, function(error, user) {
        if (user) {
          return callback(null, true);
        }
        else {
          return callback(null, false);
        }
      });
    }
    else {
      return callback(null, false);
    }
};

const server = new Hapi.Server();
server.connection({ port: 3000, host: 'localhost' });

server.route({
    method: 'GET',
    path: '/',
    config: { auth: false },
    handler: function (request, reply) {
      reply("Server working properly. Try login http://localhost:3000/login?username=admin&password=admin");
    }
});

server.route({
    method: 'GET',
    path: '/status',
    config: {
      plugins: {
       AclAuth: {
         permissions: '*'
       },
      }
    },
    handler: function (request, reply) {
      const acl = request.server.plugins['hapi-auth-acl'].acl;
      acl.userRoles( get_user_id( request, reply ), function( error, roles ){
          reply( 'User: ' + JSON.stringify( request.auth.credentials ) + ' Roles: ' + JSON.stringify( roles ) );
      });
    }
});

server.route({
    method: 'POST',
    path: '/user/register',
    config: { auth: false },
    handler: function (request, reply) {
      const acl = request.server.plugins['hapi-auth-acl'].acl;
      let data = request.payload;
      const onError = (err) => {
        return reply(boom.badRequest(err));
      };
      accountService
        .createUser(data, acl)
        .then((data) => {
          var user = data;
          jwtHelper
            .sign(user)
            .then(accessToken => reply({ user, accessToken }))
            .catch(onError);
        })
        .catch(onError);

    }
});


server.route({
    method: 'GET',
    path: '/user',
    config: {
      plugins: {
       AclAuth: {
         permissions: '*'
       },
      }
    },
    handler: function (request, reply) {
      const acl = request.server.plugins['hapi-auth-acl'].acl;
      accountService.getUser( get_user_id( request, reply ), acl).then((data) => {
        reply(data);
      });
    }
});


server.route({
    method: 'PUT',
    path: '/user',
    handler: function (request, reply) {
      const acl = request.server.plugins['hapi-auth-acl'].acl;
      let data = request.payload;
      if (!data.id) {
        data.id = request.auth.credentials.id;
      }
      const onError = (err) => {
        return reply(boom.badRequest(err));
      };
      accountService
        .updateUser(data, acl)
        .then((data) => {
          reply("User Updated Successfully");
        })
        .catch(onError);

    }
});

server.route({
    method: 'DELETE',
    path: '/user',
    handler: function (request, reply) {
      const acl = request.server.plugins['hapi-auth-acl'].acl;
      let userId = request.query.id ? request.query.id : request.auth.credentials.id;
      const onError = (err) => {
        return reply(boom.badRequest(err));
      };
      accountService
        .deleteUser(userId, acl)
        .then((data) => {
          reply("User Deleted Successfully");
        })
        .catch(onError);

    }
});

server.route({
    method: 'GET',
    path: '/clean',
    handler: function (request, reply) {
      let acl = request.server.plugins['hapi-auth-acl'].acl;
      acl.clean(function( error, res ){
          reply("Cleaned");
      });
    }
});


server.route({
    method: 'GET',
    path: '/login',
    config: { auth: false },
    handler: function (request, reply) {
      const payload = request.query;
      const acl = request.server.plugins['hapi-auth-acl'].acl;
      const onError = (err) => {
        reply(boom.badRequest(err));
      };

      sessionService
        .authenticate(payload.username, payload.password, acl)
        .then((user) => {
          jwtHelper
            .sign(user)
            .then(accessToken => reply({ user, accessToken }))
            .catch(onError);
        })
        .catch(onError);
    }
});

server.route({
    method: 'GET',
    path: '/logout',
    handler: function (request, reply) {
        reply('Hello, world!');
    }
});


server.route({
    method: 'PUT',
    path: '/allow/{user}/{role}',
    config: {
      plugins: {
       AclAuth: {
         permissions: '*',
         numPathComponents: 1
       },
      }
    },
    handler: function (request, reply) {
      let acl = request.server.plugins['hapi-auth-acl'].acl;
      acl.addUserRoles( request.params.user, request.params.role );
      reply( request.params.user + ' is a ' + request.params.role );
    }
});

server.route({
    method: 'PUT',
    path: '/disallow/{user}/{role}',
    config: {
      plugins: {
       AclAuth: {
         permissions: '*',
         numPathComponents: 1
       },
      }
    },
    handler: function (request, reply) {
      let acl = request.server.plugins['hapi-auth-acl'].acl;
      acl.removeUserRoles( request.params.user, request.params.role );
      reply( request.params.user + ' is not a ' + request.params.role + ' anymore.' );
    }
});

server.register(require('hapi-auth-jwt2'), function (err) {
    if(err){
      console.log(err);
    }
    server.auth.strategy('jwt', 'jwt', {
        key: 'sfafasdasdsdaserqw4213413123c123fasdfasdsadas',          // Never Share your secret key
        validateFunc: validate,            // validate function defined above
        verifyOptions: {
          algorithms: [ 'HS256' ]
        } // pick a strong algorithm
    });
    server.auth.default('jwt');
});

server.register({
    register: AclAuth,
    options: {
      host: '127.0.0.1',
      port: 6379,
      prefix: 'acl_',
      permissions: [
          {
              roles: 'admin',
              allows: [
                  { resources: '/user', permissions: '*' },
                  { resources: '/allow', permissions: 'put' },
                  { resources: '/disallow', permissions: 'put' }
              ]
          }, {
              roles: 'user',
              allows: [
                  { resources: '/user', permissions: 'get' },
                  { resources: '/user', permissions: 'put' },
                  { resources: '/user', permissions: 'delete' }
              ]
          }, {
              roles: 'guest',
              allows: []
          }
      ],
      rolesParents: [
        {
          role: 'guest',
          parent: 'user'
        },
        {
          role: 'user',
          parent: 'admin'
        }
      ]
    }
  }, (err) => {
        if (err) {
            throw err; // something bad happened loading the plugin
        }
        console.log("Auth Added");
    }
);

server.register({
    register: Good,
    options: {
        reporters: {
            console: [{
                module: 'good-squeeze',
                name: 'Squeeze',
                args: [{
                    response: '*',
                    log: '*'
                }]
            }, {
                module: 'good-console'
            }, 'stdout']
        }
    }
}, (err) => {

    if (err) {
        throw err; // something bad happened loading the plugin
    }

    server.start((err) => {
        if (err) {
            throw err;
        }
        server.log('info', 'Server running at: ' + server.info.uri);
    });
});

function get_user_id( request, response ) {
    return request.auth && request.auth.credentials.id.toString() || false;
}

function get_user_name( request, response ) {
    return request.auth && request.auth.credentials.username.toString() || false;
}
