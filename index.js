const Boom = require('boom');
const AclUser = require( './lib/acl-user.js' );
const redis = require('redis');
const util = require('util');
const _ = require('lodash');

const internals = {};

internals.pluginName = 'AclAuth';
internals.machineName = 'hapi-auth-acl';

exports.register = (server, options, next) => {
  const redisClient = redis.createClient(options.port, options.host, {no_ready_check: true});
  redisClient.on('ready', function(error) {
    const acl = new AclUser(redisClient, options.prefix);
    server.expose('acl', acl);
    //Permissions to roles
    if (!_.isEmpty(options.permissions)) {
      acl.allow(options.permissions);
    }
    //Defining Parent roles
    if (!_.isEmpty(options.rolesParents)) {
      _.forEach(options.rolesParents, function(value) {
        acl.addRoleParents( value.parent, value.role );
      });
    }
  });
  server.ext('onPostAuth', internals.implementation);
  next();
};

internals.implementation = (request, reply) => {
  const acl = request.server.plugins[internals.machineName].acl;
  if (!_.isEmpty(request.route.settings.plugins[internals.pluginName])) {
    let numPathComponents = request.route.settings.plugins[internals.pluginName].numPathComponents;
    let _userId,
        _actions,
        resource,
        url;
    if (!_userId) {
      if((request.auth.credentials) && (request.auth.credentials.id)){
        _userId = request.auth.credentials.id;
      } else{
        return reply(Boom.unauthorized('Access denied'));
      }
    }

    if (!_userId) {
      return reply(Boom.unauthorized('Access denied'));
    }

    url = request.route.path.split('?')[0];
    if(!numPathComponents){
      resource = url;
    }else{
      resource = url.split('/').slice(0,numPathComponents+1).join('/');
    }

    if(!_actions){
      _actions = request.method.toLowerCase();
    }

    acl.logger ? acl.logger.debug('Requesting '+_actions+' on '+resource+' by user '+_userId):null;

    acl.isAllowed(_userId, resource, _actions, function(err, allowed){
      if (err){
        return reply(Boom.unauthorized('Access denied'));
      }else if(allowed === false){
        if (acl.logger) {
          acl.logger.debug('Not allowed '+_actions+' on '+resource+' by user '+_userId);
          acl.allowedPermissions(_userId, resource, function(err, obj){
            acl.logger.debug('Allowed permissions: ' + util.inspect(obj));
          });
        }
        return reply(Boom.unauthorized('Access denied'));
      }else{
        acl.logger?acl.logger.debug('Allowed '+_actions+' on '+resource+' by user '+_userId):null;
        return reply.continue();
      }
    });
  } else {
    return reply.continue();
  }
};

exports.register.attributes = {
  pkg: require('./package.json')
};
