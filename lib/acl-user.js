const Acl = require('acl'),
    bluebird = require('bluebird'),
    contract = require('acl/lib/contract'),
    options = {
      buckets: {
        meta: 'meta',
        parents: 'parents',
        permissions: 'permissions',
        resources: 'resources',
        roles: 'roles',
        users: 'users',
        userLogins: 'userLogins',
        userDetails: 'userDetails'
      }
    },
    uuidv1 = require('uuid/v1');

//Insert hash to redis database
Acl.redisBackend.prototype.insert = function(transaction, bucket, key, value){
	contract(arguments)
      .params('object', 'string', 'string|number','string|object|array|number')
      .end();
  key = this.bucketKey(bucket, key);
  transaction.hmset(key, value);
};

//Update hash to redis database
Acl.redisBackend.prototype.update = function(transaction, bucket, key, value){
	contract(arguments)
      .params('object', 'string', 'string|number','string|object|array|number')
      .end();
  key = this.bucketKey(bucket, key);
  for (let index in value) {
    let val = value[index];
    transaction.hset(key, index, val);
  }
};

//Get Hash back from redis database on basis of key
Acl.redisBackend.prototype.select = function(bucket, key, cb){
  contract(arguments)
      .params('string', 'string|number', 'function')
      .end();
  key = this.bucketKey(bucket, key);
  this.redis.hgetall(key, cb);
};

let AclUser = function(db, prefix) {
  let redisBackend = new Acl.redisBackend( db, prefix);
  redisBackend.selectAsync = bluebird.promisify(redisBackend.select);
  return new Acl( redisBackend, this.logger(), options);
};

//Inherit all parent methods
AclUser.prototype = Acl.prototype;

//Add custom debug logger function
AclUser.prototype.logger = function() {
    return {
        debug: function( msg ) {
            console.log( '-DEBUG-', msg );
        }
    };
};

//Add New User
AclUser.prototype.addUser = function(user, cb){
  let userId = uuidv1();
  user.id  = userId;
  const _this = this;
  contract(arguments)
    .params('boolean|string|number|object|function', 'boolean|string|number|object|function' )
    .params('boolean|string|number|object|function', 'boolean|string|number|object|function', 'boolean|string|number|object|function')
    .end();
  var transaction = this.backend.begin();
  this.backend.add(transaction, this.options.buckets.userLogins, user.username, userId);
  this.backend.insert(transaction, this.options.buckets.userDetails, userId, user);
  return this.backend.endAsync(transaction).nodeify(function(error, result) {
    return _this.getUserById(userId, cb);
  });
};

//Update User Details

AclUser.prototype.updateUser = function(userId, user, cb){
  const _this = this;
  contract(arguments)
    .params('boolean|string|number|object|function', 'boolean|string|number|object|function' )
    .params('boolean|string|number|object|function', 'boolean|string|number|object|function', 'boolean|string|number|object|function')
    .end();
  return this.getUserById(userId, function(error, oldUser) {
    let transaction = _this.backend.begin();
    //if username update that too
    if ( user.username && oldUser.username != user.username) {
      _this.backend.del(transaction, _this.options.buckets.userLogins, oldUser.username);
      _this.backend.add(transaction, _this.options.buckets.userLogins, user.username, userId);
    }
    _this.backend.update(transaction, _this.options.buckets.userDetails, userId, user);
    return _this.backend.endAsync(transaction).nodeify(cb);
  });
};

//Delete User From DB

AclUser.prototype.deleteUser = function(userId, cb){
  const _this = this;
  contract(arguments)
    .params('boolean|string|number|object|function', 'boolean|string|number|object|function')
    .end();
  return this.getUserById(userId, function(error, oldUser) {
    let transaction = _this.backend.begin();
    _this.backend.del(transaction, _this.options.buckets.userLogins, oldUser.username);
    _this.backend.del(transaction, _this.options.buckets.userDetails, userId);
    return _this.backend.endAsync(transaction).nodeify(cb);
  });
};

//Get user details on basis of userId
AclUser.prototype.getUserById = function(userId, cb){
  return this.backend.selectAsync(this.options.buckets.userDetails, userId).nodeify(cb);
};

//Get user details on basis of userName
AclUser.prototype.getUserByUsername = function(userName, cb){
  const _this = this;
  return this.backend.getAsync(this.options.buckets.userLogins, userName).nodeify(function(error, userId) {
    if (userId && userId[0]) {
      return _this.getUserById(userId[0], cb);
    }
    else {
      return cb(null, null);
    }
  });
};

exports = module.exports = AclUser;
